# SDR_station

This has a collection of custom python scripts designed for an automated ground station using an RTL-SDR

## Dependencies

1. RTLSDR Driver found [here](https://github.com/rtlsdrblog/rtl-sdr-blog)
2. noaa-apt for decoding
3. predict for NOAA.

## Usage

`main.py` is designed to add cron job's for the next 24 h. Passes with minimum az angle above the threshold will be added along with a noaa-apt call for image generation.

## Installation

* Following should be installed

		sudo apt install libatlas-base-dev python-pip3
		sudo pip3 install matplotlib numpy tweepy
		mkdir /opt/ahe

* noaa-apt available [here](https://noaa-apt.mbernardi.com.ar/download.html)

* predict available [here](https://www.qsl.net/kd2bd/predict.html)
	
	Need to configure ground station coordinates for correct operation 

- map overlay resource files for noaa-apt operation
		
		cp res/ ~/res -r

- modify config.py with API keys
	
- add to user crontab: `crontab -e`
	
	prediction sceduling for 24 hours

		0 */6 * * * python3 /opt/ahe/main.py >> noaa_apt.log;/usr/bin/date >> noaa_apt.log #sat prediction scripts
		*/2 * * * * python3 /opt/ahe/temp_log.py #temp update script
		@monthly bash /home/pi/sdr_station/src/bash/update_TLE.sh

- root crontab: `sudo crontab -e`
	
	NTP time update at 0000h

		0 0 * * * timedatectl set-ntp True
# MQTT Dashboard

A customized control panel was configured using the [MQTT Dash](https://play.google.com/store/apps/details?id=net.routix.mqttdash&hl=en&gl=US) app. The `/ahe/metrics/exchange` topic contains a retained msg with the required payload. can also be found for manual configuration

	[{"mainTextSize":"LARGE","postfix":"","prefix":"","textColor":-1,"enableIntermediateState":true,"enablePub":false,"enteredIntermediateStateAt":0,"intermediateStateTimeout":0,"jsOnReceive":"","jsonPath":"","lastPayload":"28.562","qos":0,"retained":false,"topic":"/ahe/home-sdr/temp","topicPub":"","updateLastPayloadOnPub":true,"id":"ab8a945c-c097-4f4c-97a0-68118394760e","jsBlinkExpression":"","jsOnDisplay":"","jsOnTap":"","lastActivity":1614455523,"longId":1,"name":"System temp","type":1},{"mainTextSize":"SMALL","postfix":"","prefix":"","textColor":-128,"enableIntermediateState":true,"enablePub":false,"enteredIntermediateStateAt":0,"intermediateStateTimeout":5,"jsOnReceive":"","jsonPath":"","lastPayload":"124.43.89.130","qos":0,"retained":false,"topic":"/ahe/home-sdr/ip","topicPub":"/ahe/home-sdr/cmd","updateLastPayloadOnPub":false,"id":"41aace28-8849-4053-b1ed-8d9c370ac608","jsBlinkExpression":"","jsOnDisplay":"","jsOnTap":"","lastActivity":1614455488,"longId":2,"name":"IP","type":1},{"iconOff":"ic_mic_off","iconOn":"ic_mic_on","offColor":-4194240,"onColor":-12517632,"payloadOff":"killtcp","payloadOn":"tcp","enableIntermediateState":true,"enablePub":true,"enteredIntermediateStateAt":0,"intermediateStateTimeout":30,"jsOnReceive":"","jsonPath":"","lastPayload":"killtcp","qos":0,"retained":false,"topic":"/ahe/home-sdr/tcp/status","topicPub":"/ahe/home-sdr/cmd","updateLastPayloadOnPub":false,"id":"47081407-d31a-40e5-8fc5-8def90f5f9a9","jsBlinkExpression":"","jsOnDisplay":"","jsOnTap":"","lastActivity":1614454154,"longId":4,"name":"","type":2},{"iconOff":"ic_cloud_upload","iconOn":"ic_cloud_upload","offColor":-1,"onColor":-1,"payloadOff":"ip","payloadOn":"ip","enableIntermediateState":false,"enablePub":true,"enteredIntermediateStateAt":1614455487,"intermediateStateTimeout":10,"jsOnReceive":"","jsonPath":"","lastPayload":"ip","qos":0,"retained":false,"topic":"/ahe/home-sdr/ip","topicPub":"/ahe/home-sdr/cmd","updateLastPayloadOnPub":false,"id":"5001c33f-3a38-4033-b43b-44e156dba8ad","jsBlinkExpression":"","jsOnDisplay":"","jsOnTap":"","lastActivity":1614454079,"longId":6,"name":"get ip","type":2}]



# Features

- [x] rtl_fm based basic scripts
- [x] Automatic tweet/upload 
- [ ] Dynamically detect tone and start stop decoding(GNU Radio based)
- [x] Detect pass direction and rotate image as needed (testing)
- [ ] Design deployment script for full installation
- [x] MQTT Dashboard for tuning
- [x] Update predict TLE files from source
## Bash scripts

includes a debug script to check the rotation code. Not needed as in the final version
