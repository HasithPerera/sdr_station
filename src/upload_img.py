#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 31 09:48:33 2020

@author: hasith@fos.cmb.ac.lk
"""

import tweepy
import argparse
from datetime import datetime

# contains API keys
import config as cfg


if __name__=='__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument("file")
    parser.add_argument("rot")
    args = parser.parse_args()
    fname = args.file
    
    pth = fname.split('/')
    f_content = pth[-1].split('_')
    
    
    dt = datetime.fromtimestamp(int(f_content[1])+19800)
    
    # send tweet
    auth=tweepy.OAuthHandler(cfg.consumer_key,cfg.consumer_secret_key)
    auth.set_access_token(cfg.access_token,cfg.access_token_secret)
    api=tweepy.API(auth)
    
    tweet_text='{} pass on {}\nv2.1.2'.format(f_content[2],dt.strftime("%d-%m-%Y %H:%M GMT+05:30"))
    image_path =fname
    
    #Generate text tweet with media (image)
    try:
        status = api.update_with_media(image_path, tweet_text)
    #print(api.update_status(status))
    except:
        print('{},{}'.format(datetime.now().strftime("%d-%m-%Y %H:%M"),'Twitter error'))
