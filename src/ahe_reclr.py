#!/usr/bin/python3
import argparse
from datetime import datetime
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm

from PIL import Image
import cv2

#ahe_1609160666_NOAA-19_20201228_130426

if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("file")
    parser.add_argument("rot")
    args = parser.parse_args()
    fname = args.file
    #dt = datetime.now()
    im = np.array(Image.open(fname))
    
    rot  = int(args.rot)
    
    pth = fname.split('/')
    f_content = pth[-1].split('_')

    
    dt = datetime.fromtimestamp(int(f_content[1])+19800)
    
    R = im[:,:,0]
    B = R - im[:,:,2]
    map_layer = np.zeros_like(R)

    
    #extract map layer
    map_layer[np.where(B>0)]=1

    R[np.where(B>0)]=200
    
    #filter and crop noisy parts from the image 
    crp_data = R[:,1100:1125]
    
    prfl = np.sum(crp_data,axis=1)
    rng = np.where(prfl<1750)
    np.clip(R,100,np.max(R),out=R) 
    

    data_img = R[np.min(rng):np.max(rng),1200:2000] 
    map_img = map_layer[np.min(rng):np.max(rng),1200:2000]

    
    kernal = np.ones(4)
    map_img = cv2.dilate(map_img,kernal,iterations=1)
    ##rotation on the image
    
    if (rot>0):
    #    print("Rotation needed")
        plt.imshow(cv2.rotate(R[np.min(rng):np.max(rng),1200:2000],cv2.ROTATE_180),cmap='jet')
        plt.imshow(cv2.rotate(map_img,cv2.ROTATE_180),alpha=cv2.rotate(map_img,cv2.ROTATE_180),cmap='gray')
    else:
        plt.imshow(R[np.min(rng):np.max(rng),1200:2000],cmap='jet')
        plt.imshow(map_img,alpha=map_img,cmap='gray')
    
        
    
    ylim = np.max(rng)-np.min(rng)
    #plt.text(5,ylim-40 , '')
    plt.text(5,ylim-10 , 'Hasith Perera\n{} pass @ \n{}'.format(f_content[2],dt.strftime("%d-%m-%Y %H:%M GMT+05:30")),color='white',size=4)
    plt.axis('off')
    #plt.show()
    plt.savefig('{}_clr.png'.format(fname), bbox_inches='tight',pad_inches = 0,dpi=300)
    plt.close()
