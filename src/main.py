import os
import time
import subprocess

import config as cfg
from datetime import datetime

from crontab import CronTab
import re

def log_debug(text):
	with open(cfg.log_file,"a+") as fp:
		fp.write("{}\n".format(text))

def im_rotate_condition(start_az,end_az):
	start_section = start_az//90.0
	end_section = end_az//90.0

	# added as a bug fix for 1645882233,NOAA-19 (ends in 360)
	# wrap 360 to 0 deg
	if end_section ==4:
		end_section = 0
	if start_section ==4:
		start_section = 0


	if start_section == 1 and end_section == 0:
		return 1
	
	if start_section == 1 and end_section == 3:
		return 1


	if start_section == 2 and end_section == 0:
		return 1
	
	if start_section == 2 and end_section == 3:
		return 1
	return 0

def get_nextpass(sat="NOAA-18",start_time=[]):
	#execute predict -p "NOAA-18" <time>
	#returns LOS + 60s 

	if (start_time == []):
		start_time = int(time.time())
		#print("Get passes from {}".format(start_time))
		#subprocess.call(["predict","-p",sat,"{}".format(start_time)])	  

	ahe_exec = subprocess.Popen(["/usr/local/bin/predict","-p",sat,"{}".format(start_time)],stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
	stdout,stderr = ahe_exec.communicate()

	sat_details = stdout.decode().split('\n')
	max_al = 5;

	for row in sat_details[0:-1]:
		data = row.split(" ")
#		 print(data)
		
		# Added to log data in a file for debugging - imrot
		val = re.findall("\s+\d+\s", row)
		log_debug("{},{}".format(int(val[1]),int(val[2])))
		
		try:
			if int(data[6]) > max_al:
				max_al = int(data[6])
		except:
			pass
	#print(sat_details[0].split(" "))	
	AOS = int(sat_details[0].split(" ")[0])
	LOS = int(sat_details[-2].split(" ")[0])

   
	start_az = int( re.findall("\s+\d+\s", sat_details[0])[1])
	end_az =  int( re.findall("\s+\d+\s", sat_details[-2])[1])
	
#	 print(start_az,end_az)
#	 if start_az > end_az:
#		 rot = 1
#	 else:
#		 rot = 0
	
	#new rotation function
	rot = im_rotate_condition(start_az,end_az)

	data ={'AOS':AOS,
			'LOS':LOS,
			'dt':LOS-AOS,
			'time':datetime.fromtimestamp(AOS),    
			'sat':sat,
			'max_az':0,
			'im_rot':rot,
			}
	log_debug("{},{}\n".format(AOS,sat))

	if max_al > 10:
		data['max_az']=max_al
#		 print("AOS:{}\t LOS:{}\t dt:{} s\t max_al:{}".format(AOS,LOS,LOS-AOS,max_al))
#		 print(data)
		
	return data



def schedule_crone(data):
	'''Add cron jobs for the AOS to record using rtl_fm and decode using noaa-apt'''

	#print("Adding crone jobs")
	#AOS - 120 s
	dt = datetime.fromtimestamp(data['AOS']-600)
	dt_aos = 	datetime.fromtimestamp(data['AOS'])

	cron = CronTab(user=cfg.user)
	
	#print(dt)
	if dt.hour > 6 and dt.hour < 20:
		cfg.build_msg = "{}\n**{}** AOS:{} max_az:{}".format(cfg.build_msg,data['sat'],dt_aos.strftime("%H:%M"),data['max_az'])

		job1 = cron.new(command="/opt/anaconda3/bin/python /home/hasith/Projects/sdr_station/src/discord_post.py 'AOS {} in 10 min, max_az:{}' ".format(data["sat"],data['max_az']),comment="HAM-SAT")
		job1.setall(dt.minute,dt.hour,dt.day,"*","*")
		cron.write()
	

def schedule_crone_NOAA(data):
    '''Add cron jobs for the AOS to record using rtl_fm and decode using noaa-apt'''

    print("Add crone jobs")
    dt = datetime.fromtimestamp(data['AOS'])
    

    freq ={"NOAA-18":137.9125,
            "NOAA-19":137.100,
            "TEST":92.56,
            "ISS":145.800,
            }
    f_name = "{}_{}_{}".format(data['AOS'],data['sat'],dt.strftime("%Y%m%d_%H%M%S"))
    cron = CronTab(user='pi')

    ## rtl_fm record commands for NOAA and ISS
    if "NOAA" in data['sat']:
        job1 = cron.new(command="/usr/bin/timeout {} /usr/local/bin/rtl_fm -f {}M -s 170k -r 11052 -g 45 -p 0 -E wav -E deemp -F 9 -| /usr/bin/ffmpeg -f s16le -channels 1 -sample_rate 11052 -i - /opt/ahe/data/ahe_{}.wav".format(data['dt'],freq[data['sat']],f_name),comment="NOAA")
    else:
        #ISS SSTV pass
        job1 = cron.new(command="/usr/bin/timeout {} /usr/local/bin/rtl_fm -f {}M -s 170k -r 48k -g 45 -p 0 -E wav -E deemp -F 9 -| /usr/bin/ffmpeg -f s16le -channels 1 -sample_rate 48k -i - /opt/ahe/data/ahe_{}.wav".format(data['dt'],freq[data['sat']],f_name),comment="ISS")

    job1.setall(dt.minute,dt.hour,dt.day,"*","*")
    

    sat_tag = {"NOAA-18":"noaa_18","NOAA-19":"noaa_19","ISS":"iss"}
    #removed time tag since it showd better results

    if "NOAA" in data['sat']:
        #tweet upload schedued 2 mins after the pass

        job2 = cron.new(command='/opt/ahe/noaa-apt -c "telemetry" -R "no" -s {}  -m "yes" -o /opt/ahe/data/img/ahe_{}.png /opt/ahe/data/ahe_{}.wav && python3 /opt/ahe/ahe_reclr.py /opt/ahe/data/img/ahe_{}.png {} && python3 /opt/ahe/upload_img.py /opt/ahe/data/img/ahe_{}.png_clr.png im_rot:{}'.format(sat_tag[data['sat']],f_name,f_name,f_name,data['im_rot'],f_name,data['im_rot']),comment='NOAA')
        los_t = datetime.fromtimestamp(data['LOS']+120)
        job2.setall(los_t.minute,los_t.hour,los_t.day,"*","*")
    
    cron.write()


def get_allday(sat="NOAA-18",days=1,min_az=20):
	''' Get all sat passes till 12h in the future'''
	start = int(time.time())
	pred_t = start
	end_day = start+days*24*3600  #change to extend the prediction time
	while(pred_t<end_day): 
		data = get_nextpass(sat,start_time = pred_t)
		pred_t = data['LOS']+60
		
		if (data['max_az']>=min_az):
			#print(data)
			if "NOAA" in sat:
				#schedule_crone_NOAA(data)
				schedule_crone(data)
			else:
				schedule_crone(data)

from discord_post import post_to_discord


if __name__=='__main__':
	cron = CronTab('hasith')
	cron.remove_all(comment='HAM-SAT')

	cron.write()
	
	get_allday(sat="ISS",days=1,min_az=30)
	get_allday(sat="OSCAR-91",days=1,min_az=30)
	get_allday(sat="NOAA-18",days=1,min_az=30)
	get_allday(sat="NOAA-19",days=1,min_az=30)
	get_allday(sat="NOAA-15",days=1,min_az=30)
	post_to_discord(cfg.build_msg)
		

