## script to get the file names

lines=$(nl ahe.log|nl -s "."|grep "NOAA"|cut -d "." -f 1)
#echo $lines

#exit -2
old=9
k=0
loop_end=5

find_rot() {
	section_s=$(($1/90))
	section_e=$(($2/90))
	
	n0=0
	n1=1
	n2=2
	n3=3
	#echo "$section_s>$section_e"
	if [ $section_s -eq $n1 ]; then
		if [ $section_e -eq $n0 ]; then
			echo "1"
			return 1;
		fi
		
		if [ $section_e -eq $n3 ]; then
		
			echo "1"
			return 1;
		fi
	fi
	if [ $section_s -eq $n2 ]; then
		if [ $section_e -eq $n0 ]; then
			echo "1"
			return 1;
		fi
		
		if [ $section_e -eq $n3 ]; then
			echo "1"
			return 1;
		fi
	fi
	
	echo "0"
	return 0
}


for i in $lines
do
	k=$[k+1]
	
	#echo "itr: $k"
	#echo "line no:$i"


#	if [ $k -eq $loop_end ]; then
#		exit 1	
#	fi
	len=$[i-old]
	old=$[i]
	data=$(head -n $i ahe.log | tail -n $len )
	s_az=$(echo $data|sed 's/ /\n/g' |head -n 1|cut -d ',' -f 1)
	e_az=$(echo $data|sed 's/ /\n/g' |tail -n 2|head -n 1| cut -d ',' -f 1)
	
	file_nm=$(echo $data|sed 's/ /\n/g' |tail -n 1| cut -d ',' -f 1)
	#echo $file_nm
	#echo $data
	#echo "val: $s_az>$e_az"
	rot=$( find_rot $s_az $e_az )
	if [[ $rot -ne $n0 ]]; then
		$(mv ./imgs/*$file_nm* ./imgs/rot/)

#	{
	#	$(mv "./imgs/*$file_nm*" ".imgs/rot/") 
#	} || {
#		echo "Not found"
#	}
	fi

done
