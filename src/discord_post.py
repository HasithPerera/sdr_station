#! /usr/bin/env python3


from discord_webhook import DiscordWebhook
import config as cfg

import argparse

parser = argparse.ArgumentParser(
                    prog = 'Discode_post',
                    description = 'send megs to discord channel using webhook',
                    epilog = 'maintained by KE8TJE')
parser.add_argument("msg_txt")

def post_to_discord(msg_txt):
	for web_hook in cfg.web_hook_url:	
		print(web_hook)	
		webhook = DiscordWebhook(url=web_hook, content=msg_txt)
		response = webhook.execute()

if __name__=='__main__':

	args = parser.parse_args()
	post_to_discord(args.msg_txt)

