import paho.mqtt.client as mqtt
import time
import subprocess
import config as cfg
#from urllib2 import urlopen
import requests
import threading

def findmy_ip():
    return requests.get('http://ip.42.pl/raw').text

def run_rtltcp():
    ahe_exec = subprocess.call(["rtl_tcp","-a","192.168.1.250"],stdin=None,stdout=None,stderr=None)

def killall(client):
    ahe_exec = subprocess.call(["killall","rtl_tcp"],stdin=None,stdout=None,stderr=None)

    client.publish("/ahe/home-sdr/tcp/status",'killtcp')
def on_connect(client, userdata, flags, rc):
    client.subscribe("/ahe/home-sdr/cmd")

def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))
    if 'cmd'in msg.topic:
        if msg.payload.decode() == 'ip':
            client.publish("/ahe/home-sdr/ip",findmy_ip())
        if msg.payload.decode() == 'tcp':

            t = threading.Thread(target=run_rtltcp,name='rtltcp mqtt')
            t.daemon = True
            t.start()
            client.publish("/ahe/home-sdr/tcp/status",'tcp')
        
        if msg.payload.decode() == 'killtcp':
            killall(client)

if __name__=="__main__":
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message

    client.connect(cfg.mqtt, 1883, 60)

    client.loop_forever()

