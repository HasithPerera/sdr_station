#This script is for an external tempe logger to monitor the temp of a closed box

#sudo modprobe w1-gpio
#sudo modprobe w1-therm

#cat /sys/devices/w1_bus_master1/28-0116284bcaee/temperature

import paho.mqtt.publish as publish
import time
import subprocess
import config as cfg

if __name__=="__main__":
    
    ahe_exec = subprocess.Popen(["cat","/sys/devices/w1_bus_master1/28-0116284bcaee/temperature"],stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
    stdout,stderr = ahe_exec.communicate()
    
    publish.single("/ahe/home-sdr/temp", int(stdout)/1000, hostname=cfg.mqtt)

